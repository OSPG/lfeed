package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/gorilla/feeds"
)

const FILE = "/tmp/link"

type handler func(net.Conn)

func serveRSS(w http.ResponseWriter, r *http.Request) {
	file, err := os.OpenFile(FILE, os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()

	feed := &feeds.Feed{
		Title:       "feed of random things",
		Link:        &feeds.Link{Href: "http://127.0.0.1:8020"},
		Description: "random links",
	}

	scanner := bufio.NewScanner(file)
	numLines := linesNum(FILE)
	feed.Items = addLastXLinks(scanner, numLines, 10)
	rss, err := feed.ToRss()
	check(err)

	fmt.Fprintf(w, rss)
}

func addLink(c net.Conn) {
	defer c.Close()

	str, err := bufio.NewReader(c).ReadString('\n')
	check(err)

	// TODO:
	// Implement me! :D
	// When a entry is saved, it should be in JSON format, with an ID.
	/* entry := &feeds.Feed{
		Title: str
		Link:str
		Description: str
	}
	str, err = entry.ToJSON() */

	// TODO:
	// Use regex to check is str is a valid URL.
	if len(str) > 0 {
		file, err := os.OpenFile(FILE, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		check(err)

		defer file.Close()

		_, err = file.WriteString(str)
		check(err)
	}
}

func listen(port string, servFunc handler) {
	ln, err := net.Listen("tcp", port)
	check(err)

	for {
		conn, err := ln.Accept()
		check(err)

		go servFunc(conn)
	}
}

func main() {
	go listen(":8019", addLink)

	http.HandleFunc("/", serveRSS)
	http.ListenAndServe(":8020", nil)
}
