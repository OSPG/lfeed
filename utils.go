package main

import (
	"bufio"
	"log"
	"os"

	"github.com/gorilla/feeds"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func linesNum(FILE string) int {
	file, err := os.OpenFile(FILE, os.O_RDWR, 0666)
	if err != nil {
		log.Println(err)
		return 0
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	i := 0
	for scanner.Scan() {
		i++
	}
	return i
}

func addLastXLinks(scanner *bufio.Scanner, nLines, readLines int) []*feeds.Item {

	var l []*feeds.Item

	if nLines > readLines {
		for i := 0; i != nLines-readLines; i++ {
			scanner.Scan()
		}
	}

	for i := nLines - readLines; i != nLines && scanner.Scan(); i++ {
		line := scanner.Text()
		link := &feeds.Item{
			Title:       line,
			Link:        &feeds.Link{Href: line},
			Description: line,
		}
		l = append(l, link)
	}

	return l
}
